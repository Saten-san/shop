import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'shop/media/')

MEDIA_URL = '/media/'