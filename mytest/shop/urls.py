from django.conf.urls import url
from shop import views

app_name = 'shop'

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^registration/$', views.registration, name='registration'),
    url(r'^categories/$', views.category, name='category'),
    url(r'^category/(?P<category_id>[0-9])/$', views.category_detail, name='category_detail'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^cart/$', views.add_to_cart, name='cart'),
]
