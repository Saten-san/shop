from django.shortcuts import render, redirect, HttpResponse
from shop.models import Good, Tag, Category
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

from shop.forms import SignUpForm


def home(request):
    goods = Good.objects.all()
    context = {
        'goods': goods
    }
    return render(request, 'shop/goods.html', context)


def category(request):
    categories = Category.objects.all()
    context = {
        'categories': categories
    }
    return render(request, 'shop/categories.html', context)


def category_detail(request, category_id):
    goods = Good.objects.filter(category=category_id)
    category_name = Category.objects.get(pk=category_id)
    tags = Tag.objects.filter(category=category_id)
    context = {
        'goods': goods,
        'category': category_name,
        'tags': tags
    }
    return render(request, 'shop/category_detail.html', context)


def login_view(request):
    if request.POST:
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            return redirect('/shop/')
        else:
            error_message = {
                'error': 'неверный данные для входа'
            }
            return redirect('/shop/login', error_message)
    return render(request, 'shop/login.html')


def logout_view(request):
    logout(request)
    return redirect('/shop/')


def registration(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/shop/')
    else:
        form = SignUpForm()
    if request.user.is_authenticated:
        return redirect('/shop/login')
    else:
        return render(request, 'shop/registration.html', {'form': form})


def add_to_cart(request):
    if 'cart' not in request.session:
        request.session['cart'] = id
        return HttpResponse(request.session['cart'])
    return HttpResponse('cart is empty')
    # if request.user.is_authenticated:
    #     user_id = request.user.id
    #     request.session.set_expiry(60)
    #     request.session['user_id'] = user_id
    #     user = User.objects.get(pk=request.session['user_id'])
    #     message = 'Hello '+user.email
    #     return HttpResponse(message)
    # else:
    #     return HttpResponse('you have to log in')
