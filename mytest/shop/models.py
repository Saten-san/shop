from django.db import models


class Good(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name='имя товара')
    price = models.PositiveIntegerField(verbose_name='цена')
    timestamp = models.DateTimeField(auto_now=True, verbose_name='дата добавление')
    category = models.ForeignKey('Category', blank=True, null=True)
    tag = models.ManyToManyField('Tag', blank=True)
    image = models.ImageField(upload_to='images/', blank=True, null=True, verbose_name='изображение')

    class Meta:
        ordering = ['-timestamp']
        verbose_name = "Товар"
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=30, unique=True)
    category = models.ForeignKey('Category', blank=True, null=True)

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.name


class User(models.Model):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=255)
