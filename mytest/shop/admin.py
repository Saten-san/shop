from django.contrib import admin
from shop.models import Good, Category, Tag


admin.site.register(Good)
admin.site.register(Category)
admin.site.register(Tag)
